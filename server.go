package main

import (
	"flag"
	"fmt"
	"bitbucket.org/plweb/plweb-api-server/controller"
	"gopkg.in/kataras/iris.v6"
	"gopkg.in/kataras/iris.v6/adaptors/httprouter"
	"gopkg.in/kataras/iris.v6/adaptors/cors"
)

var (
	port = flag.String("port", ":8888", "port to listen to")
)

func main() {
	flag.Parse()
	app:=iris.New()
//	app.Use(logger.New(iris.Logger()))
	app.Adapt(iris.DevLogger())

app.Adapt(httprouter.New())
    	crs := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	})

	app.Adapt(crs) // this line should be added
	app.Get("/", controller.Index)
	app.Get("/course/:courseID/:lessonID", controller.GetCourse)
	app.Post("/submit/:classID/:courseID/:lessonID/:qn", controller.SubmitCode)
	fmt.Printf("listen on %s\n", *port)
	app.Listen(*port)
}
